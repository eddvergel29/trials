import { createApp } from "vue";
import App from "./pages/dashboard";

createApp(App).mount("#app");
